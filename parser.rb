require 'json'

class Parser

	 attr_reader :res

	 def initialize(input)
	 	 @input = input
	 end

	 # We get data as array of hashes and first make basic search among their values 
	 # for a complete coincidence
	 # Then if its not found, we search in ieach field of values by each word of input,
  # transformed to array, then take away duplicates. 
  # We make result an attribute to use it in next additional search methods.

  def search 
  	 inputs = @input.split(" ")
    object = File.read('data.json')
    data = JSON.parse object
    res = []
    data.each do |i|
   	  vals = i.values
      vals.each do |v|
   	    if v.include? @input
   	  	   res << i
   	    else
          inputs.each do |k|
          res << i if vals.include? k
        end
      end
     end
    end
   	p @res = res.uniq
  end
 
  # We take gathered result from different input criteria's and make them accept 
  # all of them at once to find data matching several requests.

  def search_all_fields
 	  inputs = @input.split(" ")
    p	all_fields = @res.select{|i| i.values.map{|k| k == inputs[0..1]}}
  end
end

  # NOTES:
  # 1. It is splitted into different methods for reusability.
  # 2.it could be done with regexp, kind of: 
  # res = object.search(/"(name|type|designed by)":("(\\"|[^"])*"|\[("(\\"|[^"])*"(,"(\\"|[^"])*")*)?\])/i)
  
test_1 = Parser.new("Interpreted") 
test_1.search
p "++++++++++++++++++++++++"
test_2 = Parser.new('Ada') 
test_2.search
p "++++++++++++++++++++++++"
test_3 = Parser.new('Thomas Eugene Kurtz')
test_3.search 
p "++++++++++++++++++++++++"
test_4 = Parser.new('Interpreted Thomas Eugene')
test_4.search 
p "++++++++++++++++++++++++"
test_5 = Parser.new('Compiled, Procedural, Microsoft')
test_5.search 
p "<><><><><><><><><><><><>"
test_5.search_all_fields
#[{"Name"=>"C#", "Type"=>"Compiled, Curly-bracket, Iterative, Object-oriented class-based, Reflective, Procedural", "Designed by"=>"Microsoft"}, {"Name"=>"JScript", "Type"=>"Curly-bracket, Procedural, Reflective, Scripting", "Designed by"=>"Microsoft"}, {"Name"=>"VBScript", "Type"=>"Interpreted, Procedural, Scripting, Object-oriented class-based", "Designed by"=>"Microsoft"}, {"Name"=>"Visual Basic", "Type"=>"Compiled, Procedural", "Designed by"=>"Microsoft"}, {"Name"=>"Visual FoxPro", "Type"=>"Compiled, Data-oriented, Object-oriented class-based, Procedural", "Designed by"=>"Microsoft"}, {"Name"=>"Windows PowerShell", "Type"=>"Command line interface, Curly-bracket, Interactive mode, Interpreted, Scripting", "Designed by"=>"Microsoft"}, {"Name"=>"X++", "Type"=>"Compiled, Object-oriented class-based, Procedural, Reflective", "Designed by"=>"Microsoft"}]

